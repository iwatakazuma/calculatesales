/*-----売上集計システム-----*/

package jp.alhinc.iwata_kazuma.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
/*-----支店定義ファイルの読み込みとマップへの支店コードと支店名の保持-----*/
				
		//指定のディレクトリ args[0] = "C:\Users\iwata.kazuma\Desktop\売上集計システム" コマンドライン引数
	
		//コマンドライン引数が１つのみであることの確認
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		//売上集計ファイルを格納しているディレクトリ名の宣言
		String filePath = args[0];
		//定義する対象名の宣言
		String definedName = "支店";
		//支店定義ファイル名の宣言
		String definedFile = "branch.lst";
		//売上集計ファイル名の宣言
		String totalFileName = "branch.out";
		//支店コードの正規表現の宣言
		String codePattern = "[0-9]{3}";
		//Mapの定義(key:支店コード,value:支店名)
		Map<String, String> codeAndNames = new HashMap<>();
		//Mapの定義(key:支店コード,value:売上)
		Map <String, Long> codeAndSums = new HashMap <>();
		
		

		//支店定義ファイルを入力するメソッドの呼び出し
		if(!readCodeAndNames(filePath, definedName, codeAndNames, codeAndSums, codePattern, definedFile)){
			return;
		}

/*-----売上ファイルの抽出・連番チェック→売上集計-----*/
		FilenameFilter rcdFilter = new FilenameFilter() {
			public boolean accept(File directory, String fileName) {
				File file = new File(directory, fileName);
				if(fileName.matches("[0-9]{8}.rcd") && file.isFile()) {
					return true;
				}else{
					return false;
				}
			}
		};
		List<Integer> fileNumberList = new ArrayList<Integer>();
		File[] earnFiles = new File(filePath).listFiles(rcdFilter);
		BufferedReader br = null;
		try {
			for(int i=0; i<earnFiles.length; i++) {
				String earnFileName = earnFiles[i].getName();
				String fn = earnFileName.substring(1, 8);
				int fileNumber = Integer.parseInt(fn);
				//売上ファイルの数字部分をint型で抽出し、0番目の要素から順にリストに格納
				fileNumberList.add(fileNumber);
				if(i != 0 && fileNumberList.get(i) - fileNumberList.get(i-1) != 1) {
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}
			}
			//該当ファイルの数だけ繰り返し
			for(int i=0; i<earnFiles.length; i++) { 
				FileReader fr = new FileReader(earnFiles[i]);
				br = new BufferedReader(fr);
				String line1;
				ArrayList<String> codeAndEarn = new ArrayList<String>();
				//売上ファイルの1行目(支店コード)、2行目(売上)をそれぞれ読み取る
				while((line1 = br.readLine()) != null) {
					//index0の要素→支店コード、index1の要素→売上のリストを作成
					codeAndEarn.add(line1);
				}
				String earnFileName = earnFiles[i].getName();
				if(codeAndEarn.size() != 2) {
					System.out.println(earnFileName + "のフォーマットが不正です");
					return;
				}
				//支店コードを変数codeに代入
				String code = codeAndEarn.get(0);
				if(!codeAndNames.containsKey(code)){
					System.out.println(earnFileName + "の支店コードが不正です");
					return;
				}	
				String str;
				str = codeAndEarn.get(1);
				if(!str.matches("[0-9]{1,}")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
				//売上の型をString型からlong型にキャスト
				long earnings = Long.parseLong(str);
				long longSum = codeAndSums.get(code) + earnings;
				String strSum = String.valueOf(longSum);
				if(strSum.matches("[0-9]{11,}")) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				//支店コードが重複した場合はその売上を値に加算
				codeAndSums.put(code, longSum);
			}	
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
	//売上ファイルへ出力するメソッドの呼び出し
		if(!printTotal(totalFileName, filePath, codeAndNames, codeAndSums)) {
			return;
		}
	}
					
	
	/*-----支店定義ファイルの入力のメソッドの定義-----*/
	public static boolean readCodeAndNames(String filePath,String definedName , Map<String, String> codeAndNames, Map<String, Long> codeAndSums, String codePattern, String definedFile) {
		BufferedReader br = null;
		try {	
			//Fileオブジェクトの生成・ディレクトリ＆ファイル名の指定
			File defFile = new File(filePath, definedFile);
			if(!defFile.exists()) {
				System.out.println(definedName + "定義ファイルが存在しません"); 
				return false;
			}
			FileReader fr = new FileReader(defFile);
			br = new BufferedReader(fr);
			String line;
			//br.readLineメソッドを用いて一行ごとに繰り返し挿入
			while((line = br.readLine()) != null) {
				//splitメソッドにより","の部分で分割し、配列に保持・1行目はindex0の要素・2行目はindex1の要素
				String[] codeAndName = line.split(",");
				if(codeAndName[0].matches(codePattern) && codeAndName.length == 2) {
					String code = codeAndName[0];
					String name = codeAndName[1];
					//1つ目の要素(支店コード)をキーに、2つ目の要素(支店名)を値に代入
					codeAndNames.put(code, name);
					//支店コード,0を代入しマップを初期化
					codeAndSums.put(code, 0L);
				}else {
					System.out.println(definedName + "定義ファイルのフォーマットが不正です");
					return false;
				}
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");	
					return false;
				}
			}
		}
		return true;
	}
	
					
/*-----支店コード・支店名・売上合計を売上集計ファイルに出力するメソッドの定義-----*/
	public static boolean printTotal(String totalFileName, String filePath, Map<String, String> codeAndNames, Map<String, Long> codeAndSums) {	
		BufferedWriter  bw = null;
		try {
			File sumFile = new File(filePath, totalFileName);
			FileWriter fw = new FileWriter(sumFile);
			bw = new BufferedWriter(fw);
			//全ての支店コードをkeySetメソッドにて抽出、同時に売上集計ファイルに支店名と売上合計を出力
			for(String code : codeAndNames.keySet()) {
				bw.write(code + "," + codeAndNames.get(code) + "," + codeAndSums.get(code)); 
				bw.newLine();
			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}